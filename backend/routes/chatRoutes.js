const express = require('express');
const { accessChat } = require('../controllers/chatControllers');
const { protect } = require("../middleware/authMiddleware");
const router = express.Router();

router.route('/').post(protect, accessChat);
// router.route('/').get(protect, fetchChats);
// router.route('/group').post(protect, createGroupChat);
// router.route('/group-rename').put(protect, renameGroupChat);
// router.route('/group-remove').put(protect, removeFromGroupChat);
// router.route('/group-add').put(protect, addToGroupChat);

module.exports = router;
