const express = require("express");
const dotenv = require("dotenv");
const { chats } = require("./data/data");
const { connect } = require("mongoose");
const connectDB = require("./config/db");
const colors = require("colors");
const userRoutes = require("./routes/userRoutes");
const chatRoutes = require("./routes/chatRoutes");
const { notFound, errorHandler } = require('./middlewares/errorMiddleware');

const app = express();
dotenv.config();
connectDB();
const PORT = process.env.PORT || 7000;

app.use(express.json()); // to accept JSON Data

app.get('/', (req, res) => {
    res.send("API is Running");
});

app.use('/api/user', userRoutes);
app.use('api/chat', chatRoutes);

app.use(notFound)
app.use(errorHandler)

app.listen(PORT, console.log(`Server started on PORT ${PORT}`.yellow.bold));